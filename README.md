# 10SecGame Framework #

This is a little unity framework to build small (warioWare like) 10 second games. With the framework you should be able to create one-screen puzzle-like interactive scenario, that should be solved within a given time (default 10 sec.).

The framework is part of my game design course at the cologne game lab: http://j-hansen.de/gdp. The focus of this framework is to provide an easy construction kid, that should enable students to try out game design ideas without programming everything.

Feel free to play and create stuff with it! Let me know if you succeed!
Jonas

## Credits ##

The MIT License (MIT)
Copyright (c) 2014 Jonas Hansen, pixelsix.net

The used fond and sounds are third party material, please respect the corresponding creative commons license included in the asset folder. 
* Font: Big Bottom Cartoon by Karen B. Jones
* Sounds, www.freesound.org: 151539, 211741, 243020

# Getting started #

## play the example scenes ##
* download the project files 
* open it Unity3D
* open the example scenes in /scenes
* and press play

## create your own scenes ##

start with the blank scene /scenes/_blank 

###main concept###

**Victory condition**: player needs to reach a specific score (ScoreToWin) before the time is up (GameTime)

**Create new Game Object**

* drag sprite into scene
* add a 2D Collider Component (Physics2D/BoxCollider2D) to object
* add trigger, event or always scripts (/scripts/...) to object

**trigger scripts**

* *TriggerScripts* call an event when the specific trigger condition is met.
* Trigger conditions are: Mouse Input, Keyboard Input, Collision, Trigger (Collider enters trigger), Timer
* An event call can either been send to an *EventScript* on the object itself, on another object or both.

**event scripts**

* *EventScripts* are activated by an event call from a *TriggerScripts*
* Events can activate the following: **set score** (important to reach victory condition!), change object, change sprite, destroy object, move object, rotate object, scale object, play sound, show text

**always scripts**

* *AlwaysScripts* enables simple transformations (move, rotate, scale) and drag and drop functionality without an event call or trigger.

###customize scene settings###

**Main/GameManager**

* ScoreToWin *the score the player should reach to win*
* NextLevelID *the ID of the next Scene*
  *NOTE: the scene should be added to File/Build Settings, to be able to be loaded 

**Main/GameTimer**

* GameTime *here you can change the time the player has to solve the level*
* TimeSprite *you may change the default time sprite, that scales down depending on the game time*

**Main/GUI**

Here you can change the text, text style and images that appear when you win or lose the game.

**Main/AudioSource**

* AudioClip *change the audio clip to have another background music

**Main/timer & Main/background**

feel free to change the timer and background sprite


###notes###

* Use the Sorting Layer (Background / Default / Foreground) in the SpriteRenderer Component to change sprite render order


