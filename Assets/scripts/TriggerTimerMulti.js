﻿#pragma strict

public var timeTillTrigger : float;
public var loopTimer : boolean;

public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var myTimer : float; 
private var once : boolean;

function Start () {
	myTimer = timeTillTrigger;
}

function Update () {

	if (GameManager.gameState == State.playing) {
		if(myTimer > 0) {
			myTimer -= Time.deltaTime;
			once = true;
		} else {
			if(once) {
				SendMyEvent();
				once = false;
				if(loopTimer) myTimer = timeTillTrigger;
			}
		}
	}

}

function SendMyEvent() {

	if (GameManager.gameState == State.playing) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}
	
	}

}