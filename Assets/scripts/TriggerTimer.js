﻿#pragma strict

public var timeTillTrigger : float;
public var loopTimer : boolean;

public var event : sendEvent;
public var otherObj : GameObject;

private var myTimer : float; 
private var once : boolean;

function Start () {
	myTimer = timeTillTrigger;
}

function Update () {

	if (GameManager.gameState == State.playing) {
		if(myTimer > 0) {
			myTimer -= Time.deltaTime;
			once = true;
		} else {
			if(once) {
				SendMyEvent();
				once = false;
				if(loopTimer) myTimer = timeTillTrigger;
			}
		}
	}

}

function SendMyEvent() {

	if (GameManager.gameState == State.playing) {
	
		switch(event) {
		    case sendEvent.sendEventToMe:
		        gameObject.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToOther:
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToBoth:
		    	gameObject.SendMessage("MyEvent");
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		} 
	
	}

}