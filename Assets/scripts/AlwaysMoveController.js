﻿#pragma strict
enum myMovement{movement_1, movement_2};

public var moveStyle : myMovement;
public var speed : float = 5.0;
public var rotateSpeed : float = 3.0;

private var moveDirection : Vector3 = Vector3.zero;

function Update() {

	if (GameManager.gameState == State.playing) {
	
		if(moveStyle == myMovement.movement_1) {
			
			moveDirection = Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
		
		} else if(moveStyle == myMovement.movement_2) {

			// Rotate around z - axis
			transform.Rotate(0, 0, Input.GetAxis ("Horizontal") * rotateSpeed * -1.0, Space.Self);
			
			// Move forward / backward
			moveDirection = Vector3.left * Input.GetAxis("Vertical");
			moveDirection *= speed;
		
		}

		// MOVE
		transform.Translate(moveDirection * Time.deltaTime, Space.Self);


	}
}