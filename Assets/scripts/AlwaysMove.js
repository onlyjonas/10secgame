﻿#pragma strict

public var moveSpeed : Vector2;
public var changeDirectionOnEvent : boolean;

private var myDir : float = 1.0;

function Update () {

	transform.Translate(Vector3(moveSpeed.x*myDir, moveSpeed.y*myDir,0) * Time.deltaTime);

}

function MyEvent () {
	
	if (changeDirectionOnEvent) myDir *= -1;
	
}