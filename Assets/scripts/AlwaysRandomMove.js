﻿#pragma strict

public var speed: float; 
public var randomArea : Rect = Rect (-5, 5, 10, 10);
public var timeTillTrigger : float;

private var target : Vector3; 
private var myTimer : float; 

function Start () {
	SetRandomTargetPos();
	myTimer = timeTillTrigger;
}

function Update () {

	
		// The step size is equal to speed times frame time.
		var step = speed * Time.deltaTime;
		
		// Move our position a step closer to the target.
		transform.position = Vector3.MoveTowards(transform.position, target, step);
		
		// Set new random target
		if (GameManager.gameState == State.playing) {
			if(myTimer > 0) {
				myTimer -= Time.deltaTime;
			} else {
				SetRandomTargetPos();
				myTimer = timeTillTrigger;
			}
		}
	
	
}

function SetRandomTargetPos() {

	target = Vector3(	Random.Range(randomArea.x, randomArea.x+randomArea.width), 
						Random.Range(randomArea.y, randomArea.y-randomArea.height), 0);
						
						Debug.Log(target.x +" : "+target.y);

}

