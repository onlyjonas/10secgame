﻿#pragma strict

public var trigger : myTrigger;
public var onlyOnce : boolean;
public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];
private var once : boolean;

function Start () {
	once = true;	
}

function OnMouseDown () {
	if (trigger == myTrigger.mouseDown  && once) SendMyEvent();
}

function OnMouseUp () {
	if (trigger == myTrigger.mouseUp && once) SendMyEvent();
}

function OnMouseOver () {	
	if (trigger == myTrigger.mouseOver && once) SendMyEvent();
}

function SendMyEvent() {

	if(onlyOnce) once = false;

	if (GameManager.gameState == State.playing) {
		
		if(sendEventToMe) gameObject.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}

	}

}