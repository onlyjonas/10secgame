﻿#pragma strict

var key : String = "space";
public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

function Update () {

		if (Input.GetKeyDown (key)) {
			if (GameManager.gameState == State.playing) {
				
				if(sendEventToMe) gameObject.SendMessage("MyEvent");

			    for (var obj in sendEventToOtherObj) {
					if(obj) obj.SendMessage("MyEvent");
				}
					 
			}
		
		}

}