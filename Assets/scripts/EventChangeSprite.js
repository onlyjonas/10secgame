﻿#pragma strict

public var changeToSprite : Sprite;

private var defaultSprite : Sprite;
private var spriteToggle : boolean;

function Start () {

	defaultSprite = gameObject.GetComponent(SpriteRenderer).sprite;

}

function MyEvent () {

	if (spriteToggle) {
		gameObject.GetComponent(SpriteRenderer).sprite = defaultSprite;
		spriteToggle = false;
	} else {
		gameObject.GetComponent(SpriteRenderer).sprite = changeToSprite;
		spriteToggle = true;
	}

}