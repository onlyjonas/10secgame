﻿#pragma strict

private var screenPoint : Vector3;
private var offset : Vector3;

function OnMouseDown () {

		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		var screenToWorldPoint : Vector3 = Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(screenToWorldPoint);

}

function OnMouseDrag () {
	if (GameManager.gameState == State.playing) {
		var curScreenPoint = Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		var curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		transform.position = curPosition;
	}
}