﻿#pragma strict

public var useTagDetection : boolean;
public var collisionTag : String;
public var onlyOnce : boolean;
public var sendEventToMe : boolean = true;
public var sendEventToCollisionObj : boolean;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var once : boolean;

function Start () {
	once = true;
}

function OnCollisionEnter2D(other: Collision2D) {

	if(useTagDetection) {
	
		if( other.gameObject.tag == collisionTag  && once ) {
			SendMyEvent( other.gameObject );		
			if(onlyOnce) once = false;
		}
	
	} else {
		
		if( once ) {
			SendMyEvent( other.gameObject );	
			if(onlyOnce) once = false;
		}

	}	
	
}

function SendMyEvent( otherObj : GameObject ) {

	if (GameManager.gameState == State.playing) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent");
		if(sendEventToCollisionObj) otherObj.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}
	
	}

}