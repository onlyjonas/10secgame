﻿#pragma strict

public var useTagDetection : boolean;
public var collisionTag : String;
public var onlyOnce : boolean;
public var event : sendEvent;

private var once : boolean;

function Start () {
	once = true;
}

function OnCollisionEnter2D(other: Collision2D) {

	if(useTagDetection) {
	
		if( other.gameObject.tag == collisionTag  && once ) {
			SendMyEvent( other.gameObject );		
			if(onlyOnce) once = false;
		}
	
	} else {
		
		if( once ) {
			SendMyEvent( other.gameObject );	
			if(onlyOnce) once = false;
		}

	}	
	
}

function SendMyEvent( otherObj : GameObject ) {

	if (GameManager.gameState == State.playing) {
	
		switch(event) {
		    case sendEvent.sendEventToMe:
		        gameObject.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToOther:
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToBoth:
		    	gameObject.SendMessage("MyEvent");
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		} 
	
	}

}