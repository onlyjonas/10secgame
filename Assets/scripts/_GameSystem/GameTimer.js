﻿#pragma strict
public var gameTime : float = 5;
public var timerSprite : GameObject;

private var currentTime : float;
private var defaultTimerScale : float;

function Start () {
	currentTime = gameTime;
	
	if (timerSprite != null)
	defaultTimerScale = timerSprite.transform.localScale.x;
}

function Update () {
	
	if (GameManager.gameState == State.playing) {
	
		if(currentTime > 0){
			
			currentTime -= Time.deltaTime;
			
			UpdateTimerSpriteScale();

		}
	     
	    if(currentTime <= 0){
		  gameObject.SendMessage("GameEnd");
	    }
    
    }
}

function UpdateTimerSpriteScale() {

	if (timerSprite == null) return;

	var timerSpriteWidth = currentTime/gameTime;
	
	timerSprite.transform.localScale.x = defaultTimerScale * timerSpriteWidth;

}