#pragma strict

enum State{playing, win, lose};
enum sendEvent{sendEventToMe, sendEventToOther, sendEventToBoth};
enum myTrigger{mouseUp, mouseDown, mouseOver};

public static var gameState : State;
public static var myScore : int;
public static var myWinScore : int;

public var scoreToWin : int;
public var randomNextLevel : boolean;

function Start() {
	myScore = 0;
	myWinScore = scoreToWin;
	gameState = State.playing;
}	


public function GameEnd() {
    
    if (myScore >= myWinScore) {   	
    	
    	gameState = State.win;
    	
    } else {
    	
    	gameState = State.lose;
    	
    }	
    
}


public function LoadNextLevel() {
	
	if(randomNextLevel) {
		Application.LoadLevel(Random.Range(0, Application.levelCount));
	}	else {
		var nextLevel : int = Application.loadedLevel + 1;
		if(nextLevel >= Application.levelCount) nextLevel = 0;
		Application.LoadLevel(nextLevel);
	}
	
}


public function LoadCurrentLevel() {
	Application.LoadLevel(Application.loadedLevel);
}


public function AddToScore( x : int) {
	myScore += x;
	
	if (myScore >= myWinScore) GameEnd();
}