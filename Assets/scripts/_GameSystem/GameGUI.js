﻿#pragma strict

public var textWin : String = "you win!";
public var textLose : String = "you lose!";
public var textRetry : String = "play again";
public var textNext : String = "next";
var winTexture : Texture;
var loseTexture : Texture;

public var scoreStyle  : GUIStyle;
public var alertStyle  : GUIStyle;
public var buttonStyle  : GUIStyle;

private var myEventText : String;
private var elementWidth : float;
private	var elementHeight : float;
private var xpos : float;
private var ypos : float;

function Start () {

	elementWidth =  Screen.width - Screen.width/3;
	elementHeight =  Screen.height/3;
	xpos = Screen.width/2 - elementWidth/2;
	ypos = Screen.height/2 - elementHeight/2;

}

function OnGUI () {

	var text : String = GameManager.myScore + "/" + GameManager.myWinScore;		

	GUI.Label (Rect (10, 10, 100, 20), text, scoreStyle);

	/* WIN */

	if (GameManager.gameState == State.playing) {

		GUI.Label (Rect (xpos,ypos,elementWidth,elementHeight), myEventText, alertStyle);
	
	}
	
	/* WIN */

	if (GameManager.gameState == State.win) {

		GUI.Label (Rect (xpos,ypos-elementHeight,elementWidth,elementHeight), textWin, alertStyle);

		GUI.DrawTexture(Rect(xpos,ypos,elementWidth,elementHeight), winTexture, ScaleMode.ScaleToFit);
		
		if (GUI.Button(Rect(xpos+elementWidth/4,ypos+elementHeight+elementHeight/3,elementWidth/2,elementHeight/3),textNext, buttonStyle)) {
			gameObject.SendMessage ("LoadNextLevel");
		}

	}	

	/* LOSE */

	if (GameManager.gameState == State.lose) {

		GUI.Label (Rect (xpos,ypos-elementHeight,elementWidth,elementHeight), textLose, alertStyle);

		GUI.DrawTexture(Rect(xpos,ypos,elementWidth,elementHeight), loseTexture, ScaleMode.ScaleToFit);
		
		if (GUI.Button(Rect(xpos+elementWidth/4,ypos+elementHeight+elementHeight/3,elementWidth/2,elementHeight/3),textRetry, buttonStyle)) {
			gameObject.SendMessage ("LoadCurrentLevel");
		}

	}	
		
}

function ShowText ( eventText : String ) {
	myEventText = eventText;	
}

