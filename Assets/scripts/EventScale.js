﻿#pragma strict

public var scaleTo : Vector2;
public var toggleMe : boolean;
public var speed : float = 1.0;

private var defaultScale : Vector3;
private var targetScaleTo : Vector3;
private var toggle : boolean; 

function Start () {
	
	defaultScale = transform.localScale;
	targetScaleTo = defaultScale;

}

function Update () {

	// easing	
	var x : float = ((targetScaleTo.x - transform.localScale.x) * speed);
	var y : float = ((targetScaleTo.y - transform.localScale.y) * speed);
	transform.localScale.x += x;
	transform.localScale.y += y;

}

function MyEvent () {

	if(!toggle) {
		targetScaleTo = Vector3(scaleTo.x, scaleTo.y, 1);
		if (toggleMe) toggle = true;
	} else {
		targetScaleTo = defaultScale;
		if (toggleMe) toggle = false;
	}

}