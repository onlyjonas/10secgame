﻿#pragma strict

var key : String = "space";
public var event : sendEvent;
public var otherObj : GameObject;

function Update () {

		if (Input.GetKeyDown (key)) {
			if (GameManager.gameState == State.playing) {
				
				switch(event) {
				    case sendEvent.sendEventToMe:
				        gameObject.SendMessage("MyEvent");
				        break;
				        
				    case sendEvent.sendEventToOther:
				         if(otherObj) otherObj.SendMessage("MyEvent");
				        break;
				        
				    case sendEvent.sendEventToBoth:
				    	gameObject.SendMessage("MyEvent");
				         if(otherObj) otherObj.SendMessage("MyEvent");
				        break;
					}
					 
			}
		
		}

}