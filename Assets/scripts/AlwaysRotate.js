﻿#pragma strict

public var rotationSpeed : float;
public var changeDirectionOnEvent : boolean;

private var myDir : float = 1.0;

function Update () {

	transform.Rotate(0.0 , 0.0 , rotationSpeed * 10 * myDir * Time.deltaTime);
}

function MyEvent () {
	
	if (changeDirectionOnEvent) myDir *= -1;
	
}