﻿#pragma strict
public var text : String;
public var duration : float;
private var main : GameObject; 
private var myTimer : float;
private var startTimer : boolean;

function Start () {
	main = GameObject.Find("Main");
	startTimer = false; 
}

function Update () {
	
	if(myTimer > 0) {
		myTimer -= Time.deltaTime;
	} else {
	 if (startTimer) { 
		main.SendMessage("ShowText", "");
	 	startTimer = false;
	 }
	}

}

function MyEvent () {
	main.SendMessage("ShowText", text);
	myTimer = duration;
	startTimer = true; 
}

