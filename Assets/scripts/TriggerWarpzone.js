﻿#pragma strict
public var maxX : float = 5.5;
public var minX : float = -5.5;
public var maxY : float = 5.5;
public var minY : float = -5.5;

public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

function Update () {

	/* Warp Zone */
	if(transform.position.x > maxX) {
		transform.position.x = minX;
		SendMyEvent();
	}	
	
	if(transform.position.x < minX) {
		transform.position.x = maxX;
		SendMyEvent();
	}
	
	if(transform.position.y > maxY) {
		transform.position.y = minY;
		SendMyEvent();
	}
	
	if(transform.position.y < minY) {
		transform.position.y = maxY;
		SendMyEvent();
	}
}

function SendMyEvent() {

	if (GameManager.gameState == State.playing) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}
	
	}

}