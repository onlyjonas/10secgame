﻿#pragma strict

public var prefab : Transform;
public var position : Vector2;
public var randomPosition : boolean;
public var randomArea : Rect = Rect (-5, -5, 10, 10);

function MyEvent () {

	if (randomPosition) position = Vector2(	Random.Range(randomArea.x, randomArea.x+randomArea.width), 
											Random.Range(randomArea.y, randomArea.y-randomArea.height));

	Instantiate (prefab, Vector3(position.x ,position.y, 0), prefab.rotation);

}