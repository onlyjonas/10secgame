﻿#pragma strict

public var trigger : myTrigger;
public var onlyOnce : boolean;
public var event : sendEvent;
public var otherObj : GameObject;

private var once : boolean;

function Start () {
	once = true;
}

function OnMouseDown () {
	if (trigger == myTrigger.mouseDown  && once) SendMyEvent();
}

function OnMouseUp () {
	if (trigger == myTrigger.mouseUp && once) SendMyEvent();
}

function OnMouseOver () {	
	if (trigger == myTrigger.mouseOver && once) SendMyEvent();
}

function SendMyEvent() {

	if(onlyOnce) once = false;

	if (GameManager.gameState == State.playing) {
		switch(event) {
		    case sendEvent.sendEventToMe:
		        gameObject.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToOther:
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		        
		    case sendEvent.sendEventToBoth:
		    	gameObject.SendMessage("MyEvent");
		        if(otherObj) otherObj.SendMessage("MyEvent");
		        break;
		} 
	}

}