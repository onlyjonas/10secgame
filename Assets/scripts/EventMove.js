﻿#pragma strict

public var moveRelativeTo : Vector2;
public var toggleMe : boolean;
public var speed : float = 1.0;

private var defaultPos : Vector3;
private var targetPos : Vector3;
private var toggle : boolean; 

function Start () {
	
	defaultPos = transform.position;
	targetPos = defaultPos;

}

function Update () {

	// easing	
	var x : float = ((targetPos.x - transform.position.x) * speed);
	var y : float = ((targetPos.y - transform.position.y) * speed);
	transform.position.x += x;
	transform.position.y += y;

}

function MyEvent () {

	if(!toggle) {
		targetPos = Vector3(defaultPos.x+moveRelativeTo.x, defaultPos.y+moveRelativeTo.y, defaultPos.z);
		if (toggleMe) toggle = true;
	} else {
		targetPos = defaultPos;
		if (toggleMe) toggle = false;
	}

}