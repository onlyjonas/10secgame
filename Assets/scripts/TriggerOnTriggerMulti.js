﻿#pragma strict

public var useTagDetection : boolean;
public var triggerTag : String;
public var onlyOnce : boolean;
public var sendEventToMe : boolean = true;
public var sendEventToTriggerObj : boolean;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var once : boolean;

function Start () {
	once = true;
}

function OnTriggerEnter2D(other: Collider2D) {

	if(useTagDetection) {
	
		if( other.gameObject.tag == triggerTag  && once ) {
			
			SendMyEvent( other.gameObject );
				
			if(onlyOnce) once = false;
		}
	
	} else {
		
		if( once ) {
	
			SendMyEvent( other.gameObject );
								
			if(onlyOnce) once = false;
		}

	}	
	
}

function SendMyEvent( otherObj : GameObject ) {

	if (GameManager.gameState == State.playing) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent");
		if(sendEventToTriggerObj) otherObj.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}
	
	}

}