﻿#pragma strict

public var randomTimeMin : float;
public var randomTimeMax : float;
public var loopTimer : boolean;

public var sendEventToMe : boolean = true;
public var sendEventToOtherObj : GameObject [] = new GameObject[1];

private var myTimer : float; 
private var once : boolean;

function Start () {
	myTimer = Random.Range(randomTimeMin, randomTimeMax);
}

function Update () {

	if (GameManager.gameState == State.playing) {
		if(myTimer > 0) {
			myTimer -= Time.deltaTime;
			once = true;
		} else {
			if(once) {
				SendMyEvent();
				once = false;
				if(loopTimer) myTimer = Random.Range(randomTimeMin, randomTimeMax);
			}
		}
	}

}

function SendMyEvent() {

	if (GameManager.gameState == State.playing) {
	
		if(sendEventToMe) gameObject.SendMessage("MyEvent");

	    for (var obj in sendEventToOtherObj) {
			if(obj) obj.SendMessage("MyEvent");
		}
	
	}

}